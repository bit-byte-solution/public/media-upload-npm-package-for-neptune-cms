[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![TypeScript](https://img.shields.io/badge/%3C%2F%3E-TypeScript-%230074c1.svg)](http://www.typescriptlang.org/)
[![NPM downloads](https://img.shields.io/npm/dm/@lerna-lite/cli)](https://www.npmjs.com/package/@lerna-lite/cli)
[![npm](https://img.shields.io/npm/v/@lerna-lite/cli.svg?logo=npm&logoColor=fff&label=npm)](https://www.npmjs.com/package/@lerna-lite/cli)


![Logo](/image.svg)


## File Uploder 
this package is specially developed for Neptune cms. but you can use with any laravel and react js project .  


## Installation

```sh
npm i @binary-bit-solutions/neptune-mediaupload
```
or 
```sh
yarn add @binary-bit-solutions/neptune-mediaupload
```


## How to use


```sh
import MediaUpload from '@binary-bit-solutions/neptune-mediaupload'
```
```sh
<MediaUpload show={show} onHide={handleClose.bind(this)}/>
```

## API
| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| domain | string | ``` null ``` | if your application is not in public_html directory. if your app location is public_html/test then domain="test" |
| show | bool | ``` false ``` | for open/close modal |
| onHide | func   |     | A Callback fired when the close button is clicked. If used directly inside a Modal component, the onHide will automatically be propagated up to the parent Modal onHide. |
| setMedia | func |      | A Callback is fired when the file want to use |