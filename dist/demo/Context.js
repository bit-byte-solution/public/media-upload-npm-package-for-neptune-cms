"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ApiUrl = ApiUrl;
exports.AppUrl = AppUrl;
exports.Header = Header;
exports.HtmlLang = HtmlLang;
exports.Lang = Lang;
exports.ShowToast = ShowToast;
var _reactToastify = require("react-toastify");
require("react-toastify/dist/ReactToastify.css");
function ShowToast(params) {
  switch (params.type) {
    case "error":
      _reactToastify.toast.error(params.msg, {
        position: "top-right",
        autoClose: 10000,
        hideProgressBar: false,
        closeOnClick: false,
        progress: undefined,
        theme: "colored"
      });
      break;
    case "warn":
      _reactToastify.toast.warn(params.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: false,
        progress: undefined,
        theme: "colored"
      });
      break;
    case "info":
      _reactToastify.toast.warn(params.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: false,
        progress: undefined,
        theme: "colored"
      });
      break;
    default:
      _reactToastify.toast.success(params.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: false,
        progress: undefined,
        theme: "colored"
      });
      break;
  }
}
function AppUrl(url) {
  return "/".concat(url);
  //return `/demo/${url}`;
}

function ApiUrl(url) {
  return "/api/".concat(url);
  //return `/demo/api/${url}`;
}

function Header() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  //let _token = localStorage.getItem('_token')
  var _token = document.getElementById('_token').content;
  if (content) {
    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + _token
    };
  }
  return {
    'Authorization': 'Bearer ' + _token
  };
}
function Lang() {
  var l = $("html").attr("lang");
  return {
    lang: l
  };
}
function HtmlLang() {
  return $("html").attr("lang");
}