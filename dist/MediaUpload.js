"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = MediaUpload;
var _axios = _interopRequireDefault(require("axios"));
var _react = _interopRequireDefault(require("react"));
var _reactToastify = require("react-toastify");
var _Context = require("./Context");
var _reactBootstrap = require("react-bootstrap");
var _Icons = require("./Icons");
var _sweetalert = _interopRequireDefault(require("sweetalert2"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function MediaUpload(props) {
  var _React$useState = _react["default"].useState(false),
    _React$useState2 = _slicedToArray(_React$useState, 2),
    isRefreshing = _React$useState2[0],
    setRefreshing = _React$useState2[1];
  var _React$useState3 = _react["default"].useState(false),
    _React$useState4 = _slicedToArray(_React$useState3, 2),
    isUploading = _React$useState4[0],
    setUploading = _React$useState4[1];
  var _React$useState5 = _react["default"].useState({
      infos: {},
      paginations: {},
      prev_page_url: null,
      last_page_url: null,
      from: 0,
      to: 0,
      total: 0
    }),
    _React$useState6 = _slicedToArray(_React$useState5, 2),
    datatable = _React$useState6[0],
    setDatatable = _React$useState6[1];
  var _React$useState7 = _react["default"].useState({}),
    _React$useState8 = _slicedToArray(_React$useState7, 2),
    selectedFile = _React$useState8[0],
    setSelectedFile = _React$useState8[1];
  var _React$useState9 = _react["default"].useState(""),
    _React$useState10 = _slicedToArray(_React$useState9, 2),
    query = _React$useState10[0],
    setQuery = _React$useState10[1];
  var handleStartUp = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            setRefreshing(true);
            _context.next = 3;
            return _axios["default"].patch((0, _Context.ApiUrl)("".concat(props.domain || "", "get-media-libraries?src=").concat(query)), (0, _Context.Lang)(), {
              headers: (0, _Context.Header)()
            }).then(function (response) {
              var info = response.data;
              setDatatable(_objectSpread(_objectSpread({}, datatable), {}, {
                infos: info.datatable.data,
                paginations: info.datatable.links,
                prev_page_url: info.datatable.prev_page_url,
                last_page_url: info.datatable.last_page_url,
                from: info.datatable.from,
                to: info.datatable.to,
                total: info.datatable.total
              }));
              setRefreshing(false);
            })["catch"](function (error) {
              if (error.request.status == 401) {
                location.reload();
              }
            });
          case 3:
          case "end":
            return _context.stop();
        }
      }, _callee);
    }));
    return function handleStartUp() {
      return _ref.apply(this, arguments);
    };
  }();
  var handleSearch = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(e) {
      var srcKey;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            srcKey = e.target.value;
            setQuery(srcKey);
            setRefreshing(true);
            _context2.next = 5;
            return _axios["default"].patch((0, _Context.ApiUrl)("".concat(props.domain || "", "get-media-libraries?src=").concat(srcKey)), (0, _Context.Lang)(), {
              headers: (0, _Context.Header)()
            }).then(function (response) {
              var info = response.data;
              setDatatable(_objectSpread(_objectSpread({}, datatable), {}, {
                infos: info.datatable.data,
                paginations: info.datatable.links,
                prev_page_url: info.datatable.prev_page_url,
                last_page_url: info.datatable.last_page_url,
                from: info.datatable.from,
                to: info.datatable.to,
                total: info.datatable.total
              }));
              setRefreshing(false);
            })["catch"](function (error) {
              if (error.request && error.request.status == 401) {
                location.reload();
              }
            });
          case 5:
          case "end":
            return _context2.stop();
        }
      }, _callee2);
    }));
    return function handleSearch(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var handlePagination = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(url) {
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            if (query) {
              url = "".concat(url, "&src=").concat(query);
            }
            setRefreshing(true);
            _context3.next = 4;
            return _axios["default"].patch(url, (0, _Context.Lang)(), {
              headers: (0, _Context.Header)()
            }).then(function (response) {
              var info = response.data;
              setDatatable(_objectSpread(_objectSpread({}, datatable), {}, {
                infos: info.datatable.data,
                paginations: info.datatable.links,
                prev_page_url: info.datatable.prev_page_url,
                last_page_url: info.datatable.last_page_url,
                from: info.datatable.from,
                to: info.datatable.to,
                total: info.datatable.total
              }));
              setRefreshing(false);
            })["catch"](function (error) {
              if (error == 'Error: Request failed with status code 401') {
                location.reload();
              }
            });
          case 4:
          case "end":
            return _context3.stop();
        }
      }, _callee3);
    }));
    return function handlePagination(_x3) {
      return _ref3.apply(this, arguments);
    };
  }();
  var handleUpload = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
      var form;
      return _regeneratorRuntime().wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            setUploading(true);
            form = new FormData();
            form.append('lang', (0, _Context.HtmlLang)());
            Object.values(selectedFile).map(function (obj, i) {
              form.append("file[]", obj.file);
            });
            _context4.next = 6;
            return _axios["default"].post((0, _Context.ApiUrl)("".concat(props.domain || "", "upload-files")), form, {
              headers: (0, _Context.Header)()
            }).then(function (response) {
              setUploading(false);
              var info = response.data;
              if (info.errors) {
                info.errors.map(function (error) {
                  return (0, _Context.ShowToast)({
                    type: 'error',
                    msg: error
                  });
                });
              } else if (info.success) {
                setSelectedFile({});
                handleStartUp();
              }
            })["catch"](function (error) {
              if (error.request.status == 401) {
                location.reload();
              }
            });
          case 6:
          case "end":
            return _context4.stop();
        }
      }, _callee4);
    }));
    return function handleUpload() {
      return _ref4.apply(this, arguments);
    };
  }();
  var handleDownload = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(id) {
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            window.location.href = (0, _Context.AppUrl)("secure/download-media/".concat(id));
          case 1:
          case "end":
            return _context5.stop();
        }
      }, _callee5);
    }));
    return function handleDownload(_x4) {
      return _ref5.apply(this, arguments);
    };
  }();
  var handleDelete = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee7(id) {
      return _regeneratorRuntime().wrap(function _callee7$(_context7) {
        while (1) switch (_context7.prev = _context7.next) {
          case 0:
            _sweetalert["default"].fire({
              title: "Are you sure?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "Yes!",
              cancelButtonText: "No"
            }).then( /*#__PURE__*/function () {
              var _ref7 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6(res) {
                var form;
                return _regeneratorRuntime().wrap(function _callee6$(_context6) {
                  while (1) switch (_context6.prev = _context6.next) {
                    case 0:
                      if (!res.isConfirmed) {
                        _context6.next = 6;
                        break;
                      }
                      form = new FormData();
                      form.append("id", id);
                      form.append("permission", 0);
                      _context6.next = 6;
                      return _axios["default"].post((0, _Context.ApiUrl)("".concat(props.domain || "", "delete-file")), form, {
                        headers: (0, _Context.Header)()
                      }).then(function (response) {
                        var info = response.data;
                        if (info.required == "permission") {
                          handleDeletePermission(id, info.msg);
                        } else if (info.errors) {
                          info.errors.map(function (error) {
                            return (0, _Context.ShowToast)({
                              type: "error",
                              msg: error
                            });
                          });
                        } else if (info.success) {
                          (0, _Context.ShowToast)({
                            type: 'success',
                            msg: info.success
                          });
                          handleStartUp();
                        }
                      })["catch"](function (error) {
                        if (error.request && error.request.status == 401) {
                          location.reload();
                        }
                      });
                    case 6:
                    case "end":
                      return _context6.stop();
                  }
                }, _callee6);
              }));
              return function (_x6) {
                return _ref7.apply(this, arguments);
              };
            }());
          case 1:
          case "end":
            return _context7.stop();
        }
      }, _callee7);
    }));
    return function handleDelete(_x5) {
      return _ref6.apply(this, arguments);
    };
  }();
  var handleDeletePermission = /*#__PURE__*/function () {
    var _ref8 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee9(id, msg) {
      return _regeneratorRuntime().wrap(function _callee9$(_context9) {
        while (1) switch (_context9.prev = _context9.next) {
          case 0:
            _sweetalert["default"].fire({
              title: "Permission required !",
              text: msg,
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "Yes!",
              cancelButtonText: "No"
            }).then( /*#__PURE__*/function () {
              var _ref9 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee8(res) {
                var form;
                return _regeneratorRuntime().wrap(function _callee8$(_context8) {
                  while (1) switch (_context8.prev = _context8.next) {
                    case 0:
                      if (!res.isConfirmed) {
                        _context8.next = 6;
                        break;
                      }
                      form = new FormData();
                      form.append("id", id);
                      form.append("permission", 1);
                      _context8.next = 6;
                      return _axios["default"].post((0, _Context.ApiUrl)("".concat(props.domain || "", "delete-file")), form, {
                        headers: (0, _Context.Header)()
                      }).then(function (response) {
                        var info = response.data;
                        if (info.required == "permission") {} else if (info.errors) {
                          info.errors.map(function (error) {
                            return (0, _Context.ShowToast)({
                              type: "error",
                              msg: error
                            });
                          });
                        } else if (info.success) {
                          (0, _Context.ShowToast)({
                            type: 'success',
                            msg: info.success
                          });
                          handleStartUp();
                        }
                      })["catch"](function (error) {
                        if (error.request && error.request.status == 401) {
                          location.reload();
                        }
                      });
                    case 6:
                    case "end":
                      return _context8.stop();
                  }
                }, _callee8);
              }));
              return function (_x9) {
                return _ref9.apply(this, arguments);
              };
            }());
          case 1:
          case "end":
            return _context9.stop();
        }
      }, _callee9);
    }));
    return function handleDeletePermission(_x7, _x8) {
      return _ref8.apply(this, arguments);
    };
  }();
  var handleMedia = function handleMedia(info) {
    if (props.setMedia) {
      props.setMedia(info);
    }
    setQuery("");
  };
  var handleHide = function handleHide() {
    props.onHide();
    setQuery("");
  };
  _react["default"].useEffect(function () {
    if (props.show) {
      handleStartUp();
    }
  }, [props]);
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal, {
    size: "xl",
    show: props.show,
    onHide: handleHide.bind(this),
    backdrop: "static",
    keyboard: false
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Header, {
    closeButton: true,
    className: "py-0"
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Title, {
    className: "m-0"
  }, "Media Library"), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Form, {
    style: {
      width: "80%"
    }
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Row, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Col, {
    sm: true,
    md: {
      span: 6,
      offset: 3
    }
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Form.Group, {
    controlId: "formFileSm",
    className: "my-2"
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.InputGroup, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Form.Control, {
    type: "file",
    size: "sm",
    multiple: true,
    onChange: function onChange(e) {
      var files = e.target.files;
      var selectedFiles = [];
      Object.values(files).map(function (file, i) {
        selectedFiles.push({
          file: file
        });
      });
      if (!selectedFiles) {
        setSelectedFile({});
      } else {
        setSelectedFile(selectedFiles);
      }
    }
  }), !isUploading ? /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Button, {
    type: "button",
    size: "sm",
    onClick: handleUpload.bind(this)
  }, "Upload") : /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Button, {
    disabled: true,
    type: "button",
    size: "sm"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "spinner-border spinner-border-sm me-1",
    role: "status",
    "aria-hidden": "true"
  }), "Uploading..."))))))), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Body, {
    className: "pt-0"
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Row, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Col, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "app-search dropdown d-none d-lg-block my-2"
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Form, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.InputGroup, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Form.Control, {
    type: "text",
    placeholder: "Search...",
    onChange: handleSearch.bind(this)
  }), /*#__PURE__*/_react["default"].createElement("span", {
    className: "uil uil-search-alt search-icon"
  })))))), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Row, null, isRefreshing ? /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Col, {
    className: "text-center"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "spinner-grow text-success",
    role: "status"
  })) : Object.keys(datatable.infos).length > 0 ? Object.values(datatable.infos).map(function (info, index) {
    var img = "";
    switch (info.extension) {
      case "jpeg":
      case "png":
      case "jpg":
      case "gif":
      case "svg":
      case "webp":
      case "ico":
        img = (0, _Context.AppUrl)(info.path);
        break;
      case "txt":
        img = (0, _Icons.Txt)();
        break;
      case "pdf":
        img = (0, _Icons.Pdf)();
        break;
      case "doc":
      case "docx":
        img = (0, _Icons.Docx)();
        break;
      case "xls":
      case "xlsx":
        img = (0, _Icons.Xlsx)();
        break;
      case "csv":
        img = (0, _Icons.Csv)();
        break;
      case "ppt":
      case "pptx":
        img = (0, _Icons.Ppt)();
        break;
      case "mp3":
        img = (0, _Icons.Mp3)();
        break;
      case "mp4":
      case "webm":
        img = (0, _Icons.Mp4)();
        break;
      case "zip":
        img = (0, _Icons.Zip)();
        break;
      default:
        img = (0, _Icons.FileUnknown)();
        break;
    }
    return /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Col, {
      sm: 12,
      md: 2,
      key: index
    }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Card, null, /*#__PURE__*/_react["default"].createElement("div", {
      style: {
        height: '150px'
      }
    }, /*#__PURE__*/_react["default"].createElement("img", {
      className: "card-img-top",
      alt: info.name,
      style: {
        maxHeight: "150px"
      },
      src: img
    })), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Card.Body, {
      className: "p-0 border-top border-primary"
    }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Card.Title, {
      className: "text-center",
      style: {
        height: "33px"
      }
    }, info.name), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Row, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Col, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.ButtonGroup, {
      size: "sm",
      className: "w-100"
    }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Button, {
      onClick: function onClick() {
        return handleMedia(info);
      },
      style: {
        borderRadius: "0 0 0 3px"
      }
    }, /*#__PURE__*/_react["default"].createElement("i", {
      className: "uil uil-plus"
    })), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Button, {
      variant: "warning",
      onClick: function onClick() {
        return handleDownload(info.id);
      },
      style: {
        borderRadius: "0"
      }
    }, /*#__PURE__*/_react["default"].createElement("i", {
      className: "uil uil-down-arrow"
    })), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Button, {
      variant: "danger",
      onClick: function onClick() {
        return handleDelete(info.id);
      },
      style: {
        borderRadius: "0 0 3px 0"
      }
    }, /*#__PURE__*/_react["default"].createElement("i", {
      className: "uil uil-trash-alt"
    }))))))));
  }) : /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Col, null, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-center"
  }, "No Data Found")))), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Footer, {
    className: "pb-0"
  }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Row, null, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Col, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "paging_simple_numbers float-end"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "pagination pagination-rounded"
  }, /*#__PURE__*/_react["default"].createElement("ul", {
    className: "pagination pagination-rounded"
  }, Object.keys(datatable.paginations).length > 0 ? datatable.paginations.map(function (paginate, i) {
    return /*#__PURE__*/_react["default"].createElement("li", {
      key: i,
      className: paginate.label == "&laquo; Previous" ? datatable.prev_page_url ? "paginate_button page-item previous" : "paginate_button page-item previous disabled" : paginate.label == "Next &raquo;" ? paginate.url ? "paginate_button page-item next" : "paginate_button page-item next disabled" : paginate.active ? "paginate_button page-item active" : "paginate_button page-item"
    }, paginate.label == "&laquo; Previous" ? /*#__PURE__*/_react["default"].createElement("a", {
      href: "#",
      className: "page-link",
      onClick: function onClick(e) {
        e.preventDefault();
        handlePagination(paginate.url);
      }
    }, /*#__PURE__*/_react["default"].createElement("i", {
      className: "uil uil-arrow-left"
    })) : paginate.label == "Next &raquo;" ? /*#__PURE__*/_react["default"].createElement("a", {
      href: "#",
      className: "page-link",
      onClick: function onClick(e) {
        e.preventDefault();
        handlePagination(paginate.url);
      }
    }, /*#__PURE__*/_react["default"].createElement("i", {
      className: "uil uil-arrow-right"
    })) : paginate.label == "..." ? /*#__PURE__*/_react["default"].createElement("a", {
      href: "#",
      className: "page-link",
      onClick: function onClick(e) {
        return e.preventDefault();
      }
    }, paginate.label) : !paginate.active ? /*#__PURE__*/_react["default"].createElement("a", {
      href: "#",
      className: "page-link",
      onClick: function onClick(e) {
        e.preventDefault();
        handlePagination(paginate.url);
      }
    }, paginate.label) : /*#__PURE__*/_react["default"].createElement("a", {
      href: "#",
      className: "page-link",
      onClick: function onClick(e) {
        return e.preventDefault();
      }
    }, paginate.label));
  }) : ""))))))), /*#__PURE__*/_react["default"].createElement(_reactToastify.ToastContainer, null));
}