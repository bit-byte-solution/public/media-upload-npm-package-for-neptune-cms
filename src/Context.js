import { toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'

export function ShowToast(params){
    switch (params.type) {
        case `error`:
            toast.error(params.msg, {
                position: `top-right`,
                autoClose: 10000,
                hideProgressBar: false,
                closeOnClick: false,
                progress: undefined,
                theme: `colored`
            })
            break;    
        case `warn`:
            toast.warn(params.msg, {
                position: `top-right`,
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                progress: undefined,
                theme: `colored`
            })
            break;    
        case `info`:
            toast.warn(params.msg, {
                position: `top-right`,
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                progress: undefined,
                theme: `colored`
            })
            break;    
        default:
            toast.success(params.msg, {
                position: `top-right`,
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                progress: undefined,
                theme: `colored`
            })
            break;
    }
}

export function AppUrl(url){
    return `/${url}`;
    //return `/demo/${url}`;
}

export function ApiUrl(url){
    return `/api/${url}`;
    //return `/demo/api/${url}`;
}

export function Header(content = null){
    //let _token = localStorage.getItem('_token')
    let _token = document.getElementById('_token').content
    
    if(content){
        return {
            'Content-Type' : 'application/json',
            'Accept' : 'application/json',
            'Authorization': 'Bearer '+_token
        }
    }

    return {'Authorization': 'Bearer '+_token}
}

export function Lang() {
    let l = $(`html`).attr(`lang`)
    return {lang:l}
}

export function HtmlLang() {
    return $(`html`).attr(`lang`)
}