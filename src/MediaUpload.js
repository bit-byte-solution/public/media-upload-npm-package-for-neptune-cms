import axios from 'axios'
import React from 'react'
import { ToastContainer } from 'react-toastify'
import { ApiUrl, AppUrl, Header, HtmlLang, Lang, ShowToast } from './Context'
import { Button, ButtonGroup, Card, Col, Form, InputGroup, Modal, Row } from 'react-bootstrap'
import { Csv, Docx, FileUnknown, Mp3, Mp4, Pdf, Ppt, Txt, Xlsx, Zip } from './Icons'
import Swal from 'sweetalert2'

export default function MediaUpload(props) {

    const [isRefreshing,setRefreshing] = React.useState(false)
    const [isUploading,setUploading] = React.useState(false)
    const [datatable,setDatatable] = React.useState({
        infos : {},
        paginations : {},
        prev_page_url : null,
        last_page_url : null,
        from : 0,
        to : 0,
        total : 0
    })
    
    const [selectedFile, setSelectedFile] = React.useState({})
    const [query,setQuery] = React.useState(``)

    const handleStartUp = async () => {
        setRefreshing(true)
        await axios.patch(ApiUrl(`${props.domain || ``}get-media-libraries?src=${query}`),Lang(),{headers : Header()})
        .then((response)=>{
            let info = response.data
            setDatatable({
                ...datatable,
                infos : info.datatable.data,
                paginations : info.datatable.links,
                prev_page_url : info.datatable.prev_page_url,
                last_page_url : info.datatable.last_page_url,
                from : info.datatable.from,
                to : info.datatable.to,
                total : info.datatable.total
            })
            setRefreshing(false)
        })
        .catch((error)=> {
            if(error.request.status == 401){
                location.reload()
            }
        })
    }

    const handleSearch = async (e) => {
        let srcKey = e.target.value
        setQuery(srcKey)

        setRefreshing(true)
        await axios.patch(ApiUrl(`${props.domain||``}get-media-libraries?src=${srcKey}`),Lang(),{ headers: Header() })
        .then(function(response){
            let info = response.data
            setDatatable({
                ...datatable,
                infos : info.datatable.data,
                paginations : info.datatable.links,
                prev_page_url : info.datatable.prev_page_url,
                last_page_url : info.datatable.last_page_url,
                from : info.datatable.from,
                to : info.datatable.to,
                total : info.datatable.total
            })
            setRefreshing(false)
        })
        .catch(function (error) {
            if(error.request && error.request.status == 401){
                location.reload()
            }
        })
    }

    const handlePagination = async (url) => {
        if (query) {
            url = `${url}&src=${query}`
        }
        setRefreshing(true)
        await axios.patch(url,Lang(),{ headers: Header() })
        .then((response)=>{
            let info = response.data
            setDatatable({
                ...datatable,
                infos : info.datatable.data,
                paginations : info.datatable.links,
                prev_page_url : info.datatable.prev_page_url,
                last_page_url : info.datatable.last_page_url,
                from : info.datatable.from,
                to : info.datatable.to,
                total : info.datatable.total
            })
            setRefreshing(false)
        })
        .catch((error)=> {
            if(error == 'Error: Request failed with status code 401'){
                location.reload()
            }
        })
    }

    const handleUpload = async () => {
        setUploading(true)
        const form = new FormData();
        form.append('lang', HtmlLang())
        
        Object.values(selectedFile).map((obj,i)=>{
            form.append(`file[]`, obj.file)
        })
        
        await axios.post(ApiUrl(`${props.domain||``}upload-files`),form,{headers:Header()})
        .then((response)=> {
            setUploading(false)
            let info = response.data;
            
            if(info.errors){
                (info.errors).map((error)=>(
                    ShowToast({ type : 'error', msg  : error })
                ))
            }
            else if(info.success){
                setSelectedFile({})
                handleStartUp()
            }
        })
        .catch((error)=> {
            if(error.request.status == 401){
                location.reload()
            }
        });
    }

    const handleDownload = async (id) => {
        window.location.href = AppUrl(`secure/download-media/${id}`)
    }
    
    const handleDelete = async (id) => {
        Swal.fire({
            title: `Are you sure?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Yes!`,
            cancelButtonText: `No`
        })
        .then(async (res) => {
            if (res.isConfirmed) {

                const form = new FormData()
                form.append(`id`, id)
                form.append(`permission`, 0)

                await axios.post(ApiUrl(`${props.domain || ``}delete-file`),form,{headers : Header()})
                .then(function (response) {
                    let info = response.data

                    if (info.required == `permission`) {
                        handleDeletePermission(id,info.msg)
                    }
                    else if(info.errors){
                        (info.errors).map((error)=>(
                            ShowToast({ type : `error`, msg  : error })
                        ))
                    }
                    else if(info.success){
                        ShowToast({ type : 'success', msg  : info.success })
                        handleStartUp()
                    }
                })
                .catch(function (error) {
                    if(error.request && error.request.status == 401){
                        location.reload()
                    }
                })
            }
        })
    }

    const handleDeletePermission = async (id,msg) => {
        Swal.fire({
            title: `Permission required !`,
            text: msg,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Yes!`,
            cancelButtonText: `No`
        })
        .then(async (res) => {
            if (res.isConfirmed) {

                const form = new FormData()
                form.append(`id`, id)
                form.append(`permission`, 1)

                await axios.post(ApiUrl(`${props.domain || ``}delete-file`),form,{headers : Header()})
                .then(function (response) {
                    let info = response.data

                    if (info.required == `permission`) {
                        
                    }
                    else if(info.errors){
                        (info.errors).map((error)=>(
                            ShowToast({ type : `error`, msg  : error })
                        ))
                    }
                    else if(info.success){
                        ShowToast({ type : 'success', msg  : info.success })
                        handleStartUp()
                    }
                })
                .catch(function (error) {
                    if(error.request && error.request.status == 401){
                        location.reload()
                    }
                })
            }
        })
    }

    const handleMedia = (info) => {
        if (props.setMedia) {
            props.setMedia(info)
        }
        setQuery(``)
    }

    const handleHide = () => {
        props.onHide()
        setQuery(``)
    }

    React.useEffect(()=>{
        if(props.show){
            handleStartUp()
        }
    },[props])

    return (
        <React.Fragment>
            <Modal size="xl" show={props.show} onHide={handleHide.bind(this)} backdrop="static" keyboard={false}>
                <Modal.Header closeButton className='py-0'>
                    <Modal.Title className='m-0'>Media Library</Modal.Title>
                    <Form style={{ width : `80%` }}>
                        <Row>
                            <Col sm md={{span: 6, offset: 3}}>
                                <Form.Group controlId="formFileSm" className="my-2">
                                    <InputGroup>
                                        <Form.Control type="file" size="sm" multiple
                                            onChange={e=>{
                                                const files = e.target.files;
                                                let selectedFiles = []
                                                Object.values(files).map((file,i)=>{
                                                    selectedFiles.push({file})
                                                })
                                                if (!selectedFiles){
                                                    setSelectedFile({})
                                                }
                                                else{
                                                    setSelectedFile(selectedFiles)
                                                }
                                            }}
                                        />
                                        {!isUploading ? (
                                            <Button type='button' size='sm'
                                                onClick={handleUpload.bind(this)}
                                            >Upload</Button>
                                        ) : (
                                            <Button disabled type='button' size='sm'>
                                                <span className="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>
                                                Uploading...
                                            </Button>
                                        )}
                                    </InputGroup>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Header>

                <Modal.Body className='pt-0'>
                    {/* search */}
                    <Row>
                        <Col>
                            <div className="app-search dropdown d-none d-lg-block my-2">
                                <Form>
                                    <InputGroup>
                                        <Form.Control type='text' placeholder='Search...'
                                            onChange={handleSearch.bind(this)}
                                        />
                                        <span className="uil uil-search-alt search-icon"></span>
                                    </InputGroup>
                                </Form>
                            </div>
                        </Col>
                    </Row>

                    {/* show */}
                    <Row>
                        {isRefreshing ? (<Col className='text-center'><div className="spinner-grow text-success" role="status"></div></Col>) : 
                            Object.keys(datatable.infos).length > 0 ? (
                                Object.values(datatable.infos).map((info,index)=>{
                                    let img = ``
                                    switch (info.extension) {
                                        case `jpeg`:
                                        case `png`:
                                        case `jpg`:
                                        case `gif`:
                                        case `svg`:
                                        case `webp`:
                                        case `ico`:
                                            img = AppUrl(info.path)
                                            break
                                        case `txt`:
                                            img = Txt()
                                            break
                                        case `pdf`:
                                            img = Pdf()
                                            break
                                        case `doc`:
                                        case `docx`:
                                            img = Docx()
                                            break
                                        case `xls`:
                                        case `xlsx`:
                                            img = Xlsx()
                                            break
                                        case `csv`:
                                            img = Csv()
                                            break
                                        case `ppt`:
                                        case `pptx`:
                                            img = Ppt()
                                            break
                                        case `mp3`:
                                            img = Mp3()
                                            break
                                        case `mp4`:
                                        case `webm`:
                                            img = Mp4()
                                            break
                                        case `zip`:
                                            img = Zip()
                                            break
                                        default:
                                            img = FileUnknown()
                                            break
                                    }

                                    return (
                                        <Col sm={12} md={2} key={index}>
                                            <Card>
                                                <div style={{ height : '150px'}}>
                                                    <img className='card-img-top' alt={info.name} 
                                                        style={{ maxHeight : `150px` }}
                                                        src={img} 
                                                    />
                                                </div>
                                                <Card.Body className='p-0 border-top border-primary'>
                                                    <Card.Title className='text-center' style={{ height : `33px` }}>{info.name}</Card.Title>
                                                    <Row>
                                                        <Col>
                                                            <ButtonGroup size="sm" className='w-100'>
                                                                <Button onClick={()=> handleMedia(info)} style={{ borderRadius : `0 0 0 3px` }}>
                                                                    <i className="uil uil-plus"></i>
                                                                </Button>
                                                                <Button variant="warning" onClick={()=>handleDownload(info.id)} style={{ borderRadius : `0` }}>
                                                                    <i className="uil uil-down-arrow"></i>
                                                                </Button>
                                                                <Button variant="danger" onClick={()=>handleDelete(info.id)} style={{ borderRadius : `0 0 3px 0` }}>
                                                                    <i className="uil uil-trash-alt"></i>
                                                                </Button>
                                                            </ButtonGroup>
                                                        </Col>
                                                    </Row>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    )
                                })
                            ) : (<Col><h3 className='text-center'>No Data Found</h3></Col>)
                        }
                    </Row>
                </Modal.Body>
                <Modal.Footer className='pb-0'>
                    <Row>
                        <Col>
                            <div className="paging_simple_numbers float-end">
                                <div className="pagination pagination-rounded">
                                    <ul className="pagination pagination-rounded">
                                        {
                                            Object.keys(datatable.paginations).length > 0 ? (
                                                (datatable.paginations).map((paginate,i)=>(
                                                    <li key={i} className={
                                                        (paginate.label == "&laquo; Previous") ?
                                                            (datatable.prev_page_url) ? `paginate_button page-item previous` : `paginate_button page-item previous disabled`
                                                        :(paginate.label == "Next &raquo;") ?
                                                            (paginate.url) ? `paginate_button page-item next` : `paginate_button page-item next disabled`
                                                        :(paginate.active) ? `paginate_button page-item active` : `paginate_button page-item`
                                                    }>
                                                        {
                                                            paginate.label == "&laquo; Previous" ? (
                                                                <a href="#" className="page-link"
                                                                    onClick={(e)=>{
                                                                        e.preventDefault()
                                                                        handlePagination(paginate.url)
                                                                    }}
                                                                >
                                                                    <i className="uil uil-arrow-left"></i>
                                                                </a>
                                                            )
                                                            : paginate.label == "Next &raquo;" ? (
                                                                <a href="#" className="page-link"
                                                                    onClick={(e)=>{
                                                                        e.preventDefault()
                                                                        handlePagination(paginate.url)
                                                                    }}
                                                                >
                                                                    <i className="uil uil-arrow-right"></i>
                                                                </a>
                                                            )
                                                            : paginate.label == "..." ? (
                                                                <a href="#" className="page-link"
                                                                    onClick={(e)=>(e.preventDefault())}
                                                                >
                                                                    {paginate.label}
                                                                </a>
                                                            )
                                                            :(
                                                                !paginate.active ? (
                                                                    <a href="#" className="page-link"
                                                                        onClick={(e)=>{
                                                                            e.preventDefault()
                                                                            handlePagination(paginate.url)
                                                                        }}
                                                                    >
                                                                        {paginate.label}
                                                                    </a>
                                                                ) : (
                                                                    <a href="#" className="page-link"
                                                                        onClick={(e)=>(e.preventDefault())}
                                                                    >
                                                                        {paginate.label}
                                                                    </a>
                                                                )
                                                            )
                                                        }
                                                    </li>
                                                ))
                                            ):(``)
                                        }
                                    </ul>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Modal.Footer>
            </Modal>

            <ToastContainer />
        </React.Fragment>
    )
}